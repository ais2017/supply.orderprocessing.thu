package supply.orderprocessing.thu;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

public class OrderTest {

	@Test
	public void orderCreatedWithoutException() throws Exception {
		
		Client client = new Client(0, ClientType.REGULAR_CLIENT, "test");
		ProductInOrder products[] = new ProductInOrder[1];
		
		new Order(1, client, products);
	}
	@Test
	public void constructionFailesIfOrderIDIsNegative() throws Exception {

		Client client = new Client(0, ClientType.REGULAR_CLIENT, "test");
		ProductInOrder products[] = new ProductInOrder[1];
		
		try {
			new Order(-1, client, products);
			fail();
		} catch (Exception e) {
		}
	}
	@Test
	public void constructionFailesIfClientIsNull() {

		ProductInOrder products[] = new ProductInOrder[1];
		
		try {
			new Order(1, null, products);
			fail();
		} catch (Exception e) {
		}
	}
	@Test
	public void constructionFailesIfProductsIsNull() throws Exception{

		Client client = new Client(0, ClientType.REGULAR_CLIENT, "test");

		try {
			new Order(1, client, null);
			fail();
		} catch (Exception e) {
		}
	}
	
	@Test
	public void afterOrderCreated() throws Exception {
		int orderID = 1;
		Order order;
		Client client = new Client(0, ClientType.REGULAR_CLIENT, "test");
		ProductInOrder products[] = new ProductInOrder[1];
		
		order = new Order(orderID, client, products);
		
		assertEquals(order.getOrderID(), orderID);
		assertEquals(order.getClient(), client);
		assertArrayEquals(order.getProducts(), products);
		assertEquals(order.getOrderStatus(), Status.NEW);

	}

	@Test
	public void testEquip() throws Exception {
		int orderID = 1;
		Order order;
		Client client = new Client(0, ClientType.REGULAR_CLIENT, "test");
		ProductInOrder products[] = new ProductInOrder[1];
		order = new Order(orderID, client, products);
		
		order.equip();
		
		Assert.assertEquals(order.getOrderStatus(), Status.EQUIPED);
	}
	
	@Test
	public void testErrEquip() throws Exception {
		int orderID = 1;
		Order order;
		Client client = new Client(0, ClientType.REGULAR_CLIENT, "test");
		ProductInOrder products[] = new ProductInOrder[1];
		order = new Order(orderID, client, products);
		

		order.equip();
		
		try
		{
			order.equip();
			fail();
		} catch (Exception e) {}
	}

	@Test
	public void testExtradite() throws Exception {
		int orderID = 1;
		Order order;
		Client client = new Client(0, ClientType.REGULAR_CLIENT, "test");
		ProductInOrder products[] = new ProductInOrder[1];
		order = new Order(orderID, client, products);

		order.equip();
		order.extradite();
		
		Assert.assertEquals(order.getOrderStatus(), Status.EXTRADITION);
	}
	
	@Test
	public void testErrExtradite() throws Exception {
		int orderID = 1;
		Order order;
		Client client = new Client(0, ClientType.REGULAR_CLIENT, "test");
		ProductInOrder products[] = new ProductInOrder[1];
		order = new Order(orderID, client, products);

		try {
			order.extradite();
			fail();
		} catch (Exception e){}
	}

	@Test
	public void testAnnul() throws Exception {
		int orderID = 1;
		Order order;		
		Client client = new Client(0, ClientType.REGULAR_CLIENT, "test");
		ProductInOrder products[] = new ProductInOrder[1];
		order = new Order(orderID, client, products);
		
		order.annul();
		
		Assert.assertEquals(order.getOrderStatus(), Status.ANNULMENT);
	}
	
	@Test
	public void testErrAnnul() throws Exception {
		int orderID = 1;
		Order order;		
		Client client = new Client(0, ClientType.REGULAR_CLIENT, "test");
		ProductInOrder products[] = new ProductInOrder[1];
		order = new Order(orderID, client, products);
		
		order.equip();
		order.extradite();
		order.finish();
		try {
			order.annul();
			fail();
		} catch (Exception e) {}
	}

	@Test
	public void testFinish() throws Exception {
		int orderID = 1;
		Order order;
		Client client = new Client(0, ClientType.REGULAR_CLIENT, "test");
		ProductInOrder products[] = new ProductInOrder[1];
		order = new Order(orderID, client, products);
		
		order.finish();
		
		Assert.assertEquals(order.getOrderStatus(), Status.FINISHED);
	}
	
	@Test
	public void testErrFinish() throws Exception {
		int orderID = 1;
		Order order;
		Client client = new Client(0, ClientType.REGULAR_CLIENT, "test");
		ProductInOrder products[] = new ProductInOrder[1];
		order = new Order(orderID, client, products);
		
		try
		{
			order.finish();
		} catch (Exception e) {}
	}

}
