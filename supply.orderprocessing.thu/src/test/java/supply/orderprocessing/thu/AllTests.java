package supply.orderprocessing.thu;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ ClientTest.class, OrderForEntityTest.class, OrderTest.class, ProductInOrderTest.class,
		ProductTest.class, ScenariosTest.class })
public class AllTests {

}
