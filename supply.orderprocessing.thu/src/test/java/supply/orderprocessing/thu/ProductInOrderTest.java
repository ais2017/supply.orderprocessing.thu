package supply.orderprocessing.thu;

import static org.junit.Assert.*;

import org.junit.Test;

public class ProductInOrderTest {

	@Test
	public void ProductInOrderCreatedWithoutException() throws Exception {

		new ProductInOrder(new Product(0, "test", 42), 1);
	}
	
	@Test
	public void constructionFailesIfProductIsNull() {
		
		try {
			new ProductInOrder(null, 1);
			fail();
		} catch (Exception e) {
			return;
		}
	}
	
	@Test
	public void constructionFailesIfProductQuantityIsNegative() {
		
		try {
			new ProductInOrder(new Product(0, "test", 42), -1);
			fail();
		} catch (Exception e) {
			return;
		}
	}

	@Test
	public void afterProductInOrderCreated() throws Exception {
		int productQuantity = 23;
		Product product = new Product(0, "lolo", 1);
		
		ProductInOrder prop = new ProductInOrder(product, productQuantity);
		
		assertEquals(prop.getProduct(), product);
		assertEquals(prop.getProductQuantity(), productQuantity);
	}

	@Test
	public void testSetProductQuantity() throws Exception {
		int productQuantity = 23;
		Product product = new Product(0, "lolo", 1);		
		ProductInOrder prop = new ProductInOrder(product, productQuantity);
		
		prop.setProductQuantity(123);
		
		assertEquals(prop.getProductQuantity(), 123);
	}
}
