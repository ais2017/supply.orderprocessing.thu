package supply.orderprocessing.thu;

import static org.junit.Assert.*;

import org.junit.Test;

public class ProductTest {

	@Test
	public void productCreatedWithoutException() throws Exception {

		new Product(1, "Test", 1);
	}
	@Test
	public void constructionFailesIfProductIDIsNegative() {
		
		try {
			new Product(-1, "Test", 1);
			fail();
		} catch (Exception e) {
			return;
		}
	}
	@Test
	public void constructionFailesIfProductCostIsNegative() {
		
		try {
			new Product(1, "Test", -1);
			fail();
		} catch (Exception e) {
			return;
		}
	}
	@Test
	public void constructionFailesIfProductNameIsNullOrEmpty() {
		
		try {
			new Product(1, "", 1);
			new Product(1, null, 1);
			fail();
		} catch (Exception e) {
			return;			
		}
	}

	@Test
	public void afterproductCreated() throws Exception {
		int productID = 34;
		String productName = "koko";
		int productCost = 23;

		Product product = new Product(productID, productName, productCost);
		
		assertEquals(product.getProductID(), productID);
		assertEquals(product.getProductName(), productName);
		assertEquals(product.getProductCost(), productCost);
	}
	
	@Test
	public void testSetProductName() {
		//fail("Еще не реализовано"); // TODO
	}

	@Test
	public void testSetProductCost() {
		//fail("Еще не реализовано"); // TODO
	}

	@Test
	public void testPutProduct() {
		//fail("Еще не реализовано"); // TODO
	}

	@Test
	public void testGetProduct() {
		//fail("Еще не реализовано"); // TODO
	}

}
