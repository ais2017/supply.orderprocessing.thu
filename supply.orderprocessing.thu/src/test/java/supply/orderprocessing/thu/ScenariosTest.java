/**
 * 
 */
package supply.orderprocessing.thu;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author stepanov
 *
 */
public class ScenariosTest {

	/**
	 * Тест метода для {@link supply.orderprocessing.thu.Scenarios#courier_service()}.
	 * @throws Exception 
	 */
	@Test
	public void testCourier_service() throws Exception {
		Scenarios scenarios = new Scenarios(new TestRealization());
		
		assertEquals(scenarios.courier_service(), true); 
	}

	/**
	 * Тест метода для {@link supply.orderprocessing.thu.Scenarios#new_order(supply.orderprocessing.thu.Client, supply.orderprocessing.thu.ProductInOrder[])}.
	 * @throws Exception 
	 */
	@Test
	public void testNew_order() throws Exception {
		String productNames[] = {"Lom", "Morj"};
		int productsQuant[] = {2, 55};
		Scenarios scenarios = new Scenarios(new TestRealization());
		
		assertEquals(scenarios.new_order(2, ClientType.ENTITY_CLIENT, "OOO Olenka", productNames, productsQuant), true); 
	}

	/**
	 * Тест метода для {@link supply.orderprocessing.thu.Scenarios#order_picking()}.
	 * @throws Exception 
	 */
	@Test
	public void testOrder_picking() throws Exception {
		Scenarios scenarios = new Scenarios(new TestRealization());
		
		assertEquals(scenarios.order_picking(), true);
	}

	/**
	 * Тест метода для {@link supply.orderprocessing.thu.Scenarios#payment_fixing(supply.orderprocessing.thu.OrderForEntity)}.
	 * @throws Exception 
	 */
	@Test
	public void testPayment_fixing() throws Exception {
		Scenarios scenarios = new Scenarios(new TestRealization());

		assertEquals(scenarios.payment_fixing(1), true);
	}

	/**
	 * Тест метода для {@link supply.orderprocessing.thu.Scenarios#pickup(int)}.
	 * @throws Exception 
	 */
	@Test
	public void testPickup() throws Exception {
		Scenarios scenarios = new Scenarios(new TestRealization());

		assertEquals(scenarios.pickup(1), true);
	}

	/**
	 * Тест метода для {@link supply.orderprocessing.thu.Scenarios#transport_service()}.
	 * @throws Exception 
	 */
	@Test
	public void testTransport_service() throws Exception {
		Scenarios scenarios = new Scenarios(new TestRealization());

		assertEquals(scenarios.transport_service(), true);
	}

}
