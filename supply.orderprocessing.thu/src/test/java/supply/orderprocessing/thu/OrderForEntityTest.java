package supply.orderprocessing.thu;

import static org.junit.Assert.*;

import org.junit.Test;

public class OrderForEntityTest {
	
	@Test
	public void afterOrderCreated() throws Exception {
		ProductInOrder products[];
		Client client;
		int orderID = 1;
		OrderForEntity order;
		client = new Client(0, ClientType.ENTITY_CLIENT, "test");
		products = new ProductInOrder[1];
		products[0] = new ProductInOrder(new Product(0, "test", 399), 3);
		
		order = new OrderForEntity(orderID, client, products);
		
		assertEquals(order.paymentCheck(), (int)(399 * 3 * 0.3));
	}

	@Test
	public void testPayPos() throws Exception {
		ProductInOrder products[];
		Client client;
		int orderID = 1;
		OrderForEntity order;
		client = new Client(0, ClientType.ENTITY_CLIENT, "test");
		products = new ProductInOrder[1];
		products[0] = new ProductInOrder(new Product(0, "test", 399), 3);		
		order = new OrderForEntity(orderID, client, products);
		
		order.pay(13);
		
		assertEquals(order.paymentCheck(), (int)(399 * 3 * 0.3) - 13);
	}

	@Test
	public void testPayNeg() throws Exception {
		ProductInOrder products[];
		Client client;
		int orderID = 1;
		OrderForEntity order;
		client = new Client(0, ClientType.ENTITY_CLIENT, "test");
		products = new ProductInOrder[1];
		products[0] = new ProductInOrder(new Product(0, "test", 399), 3);		
		order = new OrderForEntity(orderID, client, products);
		
		assertEquals(order.pay(1300), -((int)(399 * 3 * 0.3) - 1300));
	}
}
