package supply.orderprocessing.thu;

import static org.junit.Assert.fail;

import org.junit.Assert;
import org.junit.Test;

public class ClientTest {

	@Test
	public void clientCreatedWithoutException() throws Exception {
		
		new Client(1, ClientType.ENTITY_CLIENT, "Test");
	}
	
	@Test
	public void constructionFailesIfClientIDIsNegative() {
		
		try {
			new Client(-1, ClientType.ENTITY_CLIENT, "dsgh");
			fail();
		} catch (Exception e) {
		}
	}

	@Test
	public void constructionFailesIfClientNameIsNullOrEmpty() {
			
		try {
			new Client(1, ClientType.ENTITY_CLIENT, "");
			new Client(1, ClientType.ENTITY_CLIENT, null);
			fail();
		} catch (Exception e) {
		}
	}
	
	@Test
	public void afterConstructionClientName() throws Exception {
		int clientID = 42;
		ClientType clientType = ClientType.ENTITY_CLIENT;
		String clientName = "test";
		Client client;
		
		client = new Client(clientID, clientType, clientName);
		
		Assert.assertEquals(client.getClientID(), clientID);
		Assert.assertEquals(client.getClientType(), clientType);
		Assert.assertEquals(client.getClientName(), clientName);
	}
}
