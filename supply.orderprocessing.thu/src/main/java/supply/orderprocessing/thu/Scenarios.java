package supply.orderprocessing.thu;

import java.util.Scanner;
import java.util.Vector;

public class Scenarios {
	private BDInterface myInterface;
	private int orderID = 0;
	
	public Scenarios(BDInterface myInterface)
	{
		this.myInterface = myInterface;
	}
	
	public boolean courier_service() throws Exception {
		Vector<Order> equipedOrders = myInterface.getOrderByStatus(Status.EQUIPED);
		
		for (Order order : equipedOrders) {
			if (order.getClient().getClientType() == ClientType.REGULAR_CLIENT)
			{
				order.extradite();
				myInterface.updOrder(order);
				/* send message to courier service */
				/* send message to client about extradition date */
			}
		}
		return true;		
	}
	
	public boolean new_order(int clientID, ClientType clientType, String clientName, String productNames[], int productsQuant[]) throws Exception {
		
		Client client = myInterface.getClientByID(clientID);
		ProductInOrder products[] = new ProductInOrder[productsQuant.length];
		Product product;
		
		for (int i = 0; i < productsQuant.length; i++)
		{
			product = myInterface.getProductByName(productNames[i]);
			if (product == null)
				throw new Exception("No such product: " + productNames[i]);
			products[i] = new ProductInOrder(product, productsQuant[i]);
		}
		
		if (client == null)
		{
			myInterface.addElement(client);
		}
		orderID++;
		if (client.getClientType() == ClientType.REGULAR_CLIENT)
		{
			Order order = new Order(orderID, client, products);
			/* send message to regular client */
			myInterface.addElement(order);
		} else {
			OrderForEntity order = new OrderForEntity(orderID, client, products);
			/* send message to entity */
			myInterface.addElement(order);
		}			
		return true;
	}
	
	public boolean order_picking() throws Exception {
		Vector<Order> unequipedOrders = myInterface.getOrderByStatus(Status.NEW);
		Order order;
		int orderID;
	    Scanner scan = new Scanner(System.in);
		
		for (Order order1 : unequipedOrders) {
			System.out.println(order1.getOrderID());
		}
		
		orderID = scan.nextInt();
		
		order = myInterface.getOrderByID(orderID);
		System.out.println("ClassID = " + order.getOrderID() + "Client name = " + order.getClient().getClientName());
		
		for (ProductInOrder product : order.getProducts()) {
			System.out.println("Product " + product.getProduct().getProductName() + " - " + myInterface.getQuantyty(product.getProduct()));
			if (product.getProductQuantity() < myInterface.getQuantyty(product.getProduct()))
			{
				myInterface.decQuantyty(product.getProduct(), product.getProductQuantity());
			} else
				return false;
		}
		order.equip();
		myInterface.updOrder(order);
		/* send message to logistics service */
		
		return true;		
	}
	
	public boolean payment_fixing(int orderID) {
		OrderForEntity order = (OrderForEntity) myInterface.getOrderByID(orderID);
		if (order.paymentCheck() == 0)
		{
			/* send message to manager */
		}
		return true;		
	}
	
	public boolean pickup(int orderID) throws Exception {		
		Order order = myInterface.getOrderByID(orderID);
		
		for (ProductInOrder product : order.getProducts()) {
			System.out.println("Product = " + product.getProduct().getProductName() + ",quantity = " + product.getProductQuantity());
		}
		order.finish();
		myInterface.updOrder(order);
		return true;		
	}
	
	public boolean transport_service() throws Exception {
		Vector<Order> equipedOrders = myInterface.getOrderByStatus(Status.EQUIPED);
		
		for (Order order : equipedOrders) {
			if (order.getClient().getClientType() == ClientType.ENTITY_CLIENT)
			{
				order.extradite();
				myInterface.updOrder(order);
				/* send message to transport service */
				/* send message to entity about extradition date */
			}
		}
		
		return true;		
	}
}
