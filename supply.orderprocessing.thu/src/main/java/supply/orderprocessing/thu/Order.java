/**
 * 
 */
package supply.orderprocessing.thu;

/**
 * @author stepanov
 *
 */
public class Order {
	private int orderID;
	private Client client;
	private ProductInOrder products[];
	private Status orderStatus;
	
	public Order(int orderID, Client client, ProductInOrder products[]) throws Exception {
		
		if (orderID < 0)
			throw new Exception("orderID mustn't be negative");
		if (client == null)
			throw new Exception("cluent mustn't be null");
		if (products == null)
			throw new Exception("products mustn't be null");
		
		this.orderID = orderID;
		this.client = client;
		this.products = products;
		orderStatus = Status.NEW;
	}
	
	/**
	 * @return orderID
	 */
	public int getOrderID() {
		return orderID;
	}
	
	/**
	 * @return client
	 */
	public Client getClient() {
		return client;
	}
	
	/**
	 * @return products
	 */
	public ProductInOrder[] getProducts() {
		return products;
	}
	
	/**
	 * @return orderStatus
	 */
	public Status getOrderStatus() {
		return orderStatus;
	}
	
	/**
	 * @return orderStatus
	 * @throws Exception 
	 */
	public void equip() throws Exception {
		
		if (this.orderStatus != Status.NEW)
			throw new Exception("Order status not NEW");
		
		this.orderStatus = Status.EQUIPED;
	}
	
	/**
	 * @throws Exception 
	 */
	public void extradite() throws Exception {
		
		if (this.orderStatus != Status.EQUIPED)
			throw new Exception("Order status not EQUIPED");
		
		this.orderStatus = Status.EXTRADITION;
	}
	
	/**
	 * @throws Exception 
	 */
	public void annul() throws Exception {
		
		if (this.orderStatus == Status.FINISHED)
			throw new Exception("Order already FINISHED");
		
		this.orderStatus = Status.ANNULMENT;
	}
	
	/**
	 * @throws Exception 
	 */
	public void finish() throws Exception {
		
		if (this.orderStatus == Status.ANNULMENT)
			throw new Exception("Order already ANNULMENT");
			
		this.orderStatus = Status.FINISHED;
	}
}
