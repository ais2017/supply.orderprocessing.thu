/**
 * 
 */
package supply.orderprocessing.thu;

/**
 * @author stepanov
 *
 */
public class Product {
	private int productID;
	private String productName;
	private int productCost;
	
	/**
	 * Constructor
	 * @param productID
	 * @param productName
	 * @param productCost
	 * @throws Exception 
	 */
	public Product(int productID, String productName, int productCost) throws Exception {
		
		if (productID < 0)
			throw new Exception("productID mustn't be negative");
		if (productName.equals(null))
			throw new Exception("productName mustn't be null");
		if (productName.equals(""))
			throw new Exception("productName mustn't be empty string");
		if (productCost < 0)
			throw new Exception("productCost mustn't be negative");
		
		this.productID = productID;
		this.productName = productName;
		this.productCost = productCost;
	}
	
	/**
	 * @return productID
	 */
	public int getProductID() {
		return productID;
	}
	
	/**
	 * @return productName
	 */
	public String getProductName() {
		return productName;
	}
	
	/**
	 * @param productName задаваемое productName
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}
	
	/**
	 * @return productCost
	 */
	public int getProductCost() {
		return productCost;
	}
	
	/**
	 * @param productCost задаваемое productCost
	 */
	public void setProductCost(int productCost) {
		this.productCost = productCost;
	}
	
	public void putProduct(int quantity) {
		
	}
	public void getProduct(int quantity) {
		
	}
}
