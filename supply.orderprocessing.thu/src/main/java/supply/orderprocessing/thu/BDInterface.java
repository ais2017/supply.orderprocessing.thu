package supply.orderprocessing.thu;

import java.util.Vector;

public interface BDInterface {
	public void addElement(Order order);
	public void addElement(Client client);
	public Vector<Order> getOrderByStatus(Status status);
	public Order getOrderByID(int orderID);
	public int getQuantyty(Product product);
	public void decQuantyty(Product product, int productQuantity);
	public void updOrder(Order order);
	public Client getClientByID(int clientID);
	public Product getProductByName(String string);
}
