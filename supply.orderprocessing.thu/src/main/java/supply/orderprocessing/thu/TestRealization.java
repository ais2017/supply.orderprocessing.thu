package supply.orderprocessing.thu;

import java.util.Vector;
import java.util.HashMap;
import java.util.Map;

public class TestRealization implements BDInterface {
	private Vector<Order> orders = new Vector<Order>();
	private Vector<Client> clients = new Vector<Client>();
	private Vector<Product> products = new Vector<Product>();
	private Map<Integer, Integer> vault = new HashMap<Integer, Integer>();

	public TestRealization() throws Exception {
		ProductInOrder productss[] = new ProductInOrder[1];
		
		clients.addElement(new Client(1, ClientType.ENTITY_CLIENT, "Roga & kopyta"));
		clients.addElement(new Client(2, ClientType.ENTITY_CLIENT, "OOO Olenka"));
		clients.addElement(new Client(3, ClientType.REGULAR_CLIENT, "Kostyl"));
		clients.addElement(new Client(4, ClientType.REGULAR_CLIENT, "Sipliy"));
		clients.addElement(new Client(5, ClientType.REGULAR_CLIENT, "Bobuk"));
		clients.addElement(new Client(6, ClientType.REGULAR_CLIENT, "Fenya"));
		products.addElement(new Product(1, "Kostyl", 34));
		products.addElement(new Product(2, "Lom", 34));
		products.addElement(new Product(3, "Dirokol", 34));
		products.addElement(new Product(4, "Dirokol2", 34));
		products.addElement(new Product(5, "Morj", 543));
		products.addElement(new Product(6, "Kolya", 34));
		productss[0] = new ProductInOrder(products.get(3), 5);
		orders.addElement(new OrderForEntity(1, clients.get(0), productss));
		orders.addElement(new Order(2, clients.get(3), productss));
		orders.addElement(new Order(3, clients.get(5), productss));
		orders.addElement(new Order(4, clients.get(4), productss));
		orders.addElement(new Order(5, clients.get(3), productss));
		orders.addElement(new Order(6, clients.get(2), productss));
		orders.addElement(new OrderForEntity(7, clients.get(1), productss));
		vault.put(1, 1235);
		vault.put(2, 1235);
		vault.put(3, 1235);
		vault.put(4, 1235);
		vault.put(5, 1235);
		vault.put(6, 1235);
	}
	
	public void addElement(Order order) {
		orders.addElement(order);		
	}

	public void addElement(Client client) {
		clients.addElement(client);
	}

	public Vector<Order> getOrderByStatus(Status status) {
		Vector<Order> orders = new Vector<Order>();
		
		for (Order order : this.orders) {
			if (order.getOrderStatus() == status)
				orders.addElement(order);
		}
		return orders;
	}

	public Order getOrderByID(int orderID) {
		
		for (Order order : this.orders) {
			if (order.getOrderID() == orderID)
				return order;
		}
		
		return null;
	}

	public int getQuantyty(Product product) {		
		return vault.get(product.getProductID());
	}

	public void decQuantyty(Product product, int productQuantity) {
		vault.put(product.getProductID(), productQuantity);		
	}

	public void updOrder(Order order) {

		for (Order order1 : this.orders) {
			if (order1.getOrderID() == order.getOrderID())
				order1 = order;
				return;
		}
	}

	public Client getClientByID(int clientID) {

		for (Client client : clients) {
			if (client.getClientID() == clientID)
				return client;
		}
		
		return null;
	}

	public Product getProductByName(String string) {

		for (Product product : products) {
			if (product.getProductName() == string)
				return product;
		}
		
		return null;
	}

}
