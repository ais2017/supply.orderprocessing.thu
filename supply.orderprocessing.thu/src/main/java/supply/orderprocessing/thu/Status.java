/**
 * 
 */
package supply.orderprocessing.thu;

/**
 * @author stepanov
 *
 */
public enum Status {
	NEW, EQUIPED, EXTRADITION, ANNULMENT, FINISHED;
}
