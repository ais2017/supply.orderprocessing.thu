/**
 * 
 */
package supply.orderprocessing.thu;
/**
 * @author stepanov
 *
 */
public class Client {
	private int clientID;
	private ClientType clientType;
	private String clientName;
	
	/**
	 * Constructor
	 * @param clientID
	 * @param clientType
	 * @param clientName
	 * @throws Exception 
	 */
	public Client(int clientID, ClientType clientType, String clientName) throws Exception {
		
		if (clientName.equals(null))
			throw new Exception("clientType mustn't be null");
		if (clientName.equals(""))
			throw new Exception("clientType mustn't be empty string");
		if (clientID < 0)
			throw new Exception("clientID must be grater than zero");
		
		
		this.clientID = clientID;
		this.clientType = clientType;
		this.clientName = clientName;
	}
	
	/**
	 * @return clientID
	 */
	public int getClientID() {
		return clientID;
	}
	
	/**
	 * @return clientType
	 */
	public ClientType getClientType() {
		return clientType;
	}
	
	/**
	 * @return clientName
	 */
	public String getClientName() {
		return clientName;
	}
	
	/**
	 * @param clientName задаваемое clientName
	 */
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
}
