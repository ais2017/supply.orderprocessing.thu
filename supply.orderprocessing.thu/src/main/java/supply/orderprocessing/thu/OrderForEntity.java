/**
 * 
 */
package supply.orderprocessing.thu;

/**
 * @author stepanov
 *
 */
public class OrderForEntity extends Order {
	private int remaining;
	
	/**
	 * @param orderID
	 * @param client
	 * @param products
	 * @throws Exception
	 */
	public OrderForEntity(int orderID, Client client, ProductInOrder[] products) throws Exception {
		
		super(orderID, client, products);
		for (ProductInOrder productInOrder : products) {
			remaining += productInOrder.getProduct().getProductCost() * productInOrder.getProductQuantity();			
		}
		remaining *= 0.3;
	}
	
	/**
	 * @param payment
	 */
	public int pay(int remaining) {
		int tempRem;
		
		this.remaining -= remaining;
		if (this.remaining < 0)
		{
			tempRem = -this.remaining;
			this.remaining = 0;
			return tempRem;
		}
		return 0; 
	}
	
	/**
	 * @return
	 */
	public int paymentCheck() {
		return remaining;
	}
}
