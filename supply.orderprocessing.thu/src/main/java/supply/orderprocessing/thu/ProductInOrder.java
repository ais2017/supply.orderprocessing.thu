/**
 * 
 */
package supply.orderprocessing.thu;

/**
 * @author stepanov
 *
 */
public class ProductInOrder {
	private Product product;
	private int productQuantity;
	
	/**
	 * Constructor
	 * @param product
	 * @param productQuantity
	 * @throws Exception 
	 */
	public ProductInOrder(Product product, int productQuantity) throws Exception {
		
		if (product == null)
			throw new Exception("product mustn't be null");
		if (productQuantity <= 0)
			throw new Exception("productQuantity must be grater than zero");
		
		this.product = product;
		this.productQuantity = productQuantity;
	}
	
	/**
	 * @return product
	 */
	public Product getProduct() {
		return product;
	}
	
	/**
	 * @return productQuantity
	 */
	public int getProductQuantity() {
		return productQuantity;
	}
	
	/**
	 * @param productQuantity задаваемое productQuantity
	 */
	public int setProductQuantity(int productQuantity) {
		this.productQuantity = productQuantity;
		return this.productQuantity;
	}
}
